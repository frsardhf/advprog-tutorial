package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        setGuild(guild);
        //ToDo: Complete Me
    }

    //ToDo: Complete Me
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
