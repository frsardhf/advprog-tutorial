package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithBarrier() {}

    public String defend() {
        return "Cast a Barrier";
    }

    public String getType() {
        return "Defend With Barrier";
    }
}
