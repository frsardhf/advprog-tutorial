package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me
    public AttackWithSword() {}

    public String attack() {
        return "Swing a Sword";
    }

    public String getType() {
        return "Attack With Sword";
    }
}
