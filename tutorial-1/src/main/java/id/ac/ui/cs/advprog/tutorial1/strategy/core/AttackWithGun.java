package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me
    public AttackWithGun() {}

    public String attack() {
        return "Shoot with Gun";
    }

    public String getType() {
        return "Attack With Gun";
    }
}
