package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        int order;
        for (int i = spells.size(); i >= 0; i--) {
            order = i - 1;
            spells.get(order).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
